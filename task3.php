<?php
class MyCalculator{
    public $value_1;
    public $value_2;

    public function __construct($value_1,$value_2)
    {
        $this->value_1=$value_1;
        $this->value_2=$value_2;
    }

    public function addition(){
        $result=$this->value_1 + $this->value_2;
         return $result ."<br>";
    }

    public function multiplication(){
        $result=$this->value_1 * $this->value_2;
        return $result ."<br>";
    }

    public function subtraction(){
        $result=$this->value_1 - $this->value_2;
        return $result ."<br>";
    }

    public function division(){
        $result=$this->value_1 / $this->value_2;
        return $result ."<br>";
    }


}
$mycalc = new MyCalculator(12,6);

echo $mycalc->addition();
echo $mycalc->multiplication();
echo $mycalc->subtraction();
echo $mycalc->division();
?>