<?php

 class factorial_of_a_number

 {

 protected $_number;

public function __construct($number)

{

         if (!is_int($number))

             {

             throw new InvalidArgumentException('Not a number or missing argument');

 }

 $this->_number = $number;

 }

 public function result(){
     $result=1;
     for ($i=$this->_number;$i>=1;$i--){
         $result=$result*$i;
     }
     return $this->_number=$result;

 }

}

$newfactorial = New factorial_of_a_number(5);

echo $newfactorial->result();

?>